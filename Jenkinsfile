/*
   Create the kubernetes namespace
*/
def createNamespace (namespace) {
   echo "Creating namespace ${namespace} if needed"
   sh "[ ! -z \"\$(kubectl get ns ${namespace} -o name 2>/dev/null)\" ] || kubectl create ns ${namespace}"
}
pipeline {
 agent any
 tools {
   jdk "openjdk"
   maven "maven"
   gradle "gradle"
 }
 parameters {
   string(name: 'tag', defaultValue: 'latest', description: 'tag {YYYYMMDD}{HHMMSS}')
   string(name: 'buildtag', defaultValue: 'v1', description: 'tag {YYYYMMDD}{HHMMSS}')
   string(name: 'GIT_URL', defaultValue: 'git@gitlab.example.cloud:root/spring-petclinic.git', description: 'GIT_URL')
   booleanParam(name: 'VERBOSE', defaultValue: false, description: '')
 }
 environment {
   GIT_BRANCH = 'main'
   GITLAB_CREDENTIAL_ID = 'gitlab-sshkey'
   IMAGE_REGISTRY = 'registry.example.cloud:8443'
   IMAGE_PROJECT = 'cicd_pipeline'
   IMAGE_NAME = 'spring-petclinic'
   IMAGE_REPO = "${IMAGE_REGISTRY}/${IMAGE_PROJECT}/${IMAGE_NAME}"
   DOCKER_USERNAME = 'jenkinsuser'
   DOCKER_PASSWORD = 'jenkins1234!'
   K8S_NAMESPACE = 'spring-boot'
	K8S_IMAGE_PULL_SECRET = 'spring-image-secret'
	VERBOSE_FLAG = '-q'
 }
 stages{
   stage('Preparation') {
     steps{
       script{
         env.ymd = sh (returnStdout: true, script: ''' echo `date '+%Y%m%d-%H%M%S'` ''')
       }
       echo("params : ${env.ymd} " + params.tag)
     }
   }
   stage('Checkout') {
     steps{
       git(branch: "${env.GIT_BRANCH}", credentialsId: "${env.GITLAB_CREDENTIAL_ID}", url: params.GIT_URL, changelog: false, poll: false)
     }
   }
   stage('Build'){
     steps{
       sh 'mvn clean package'
     }
   }
   stage('Docker build') {
     steps {
       script {
         env.IMAGE_TAG = "${params.tag}"
         env.IMAGE_LOC = env.IMAGE_REPO + ':' + env.IMAGE_TAG
       }
       sh "rm -rf docker ; mkdir docker"
       sh "cp target/spring-petclinic-2.6.0-SNAPSHOT.jar docker/spring-petclinic.jar"
       sh "cp Dockerfile docker/"
       sh "chmod -R 775 docker"
       dir('docker') {
         sh "docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD} ${IMAGE_REGISTRY}"
         echo "docker build to ${IMAGE_LOC}"
         sh "docker build -t ${IMAGE_LOC} ${env.VERBOSE_FLAG} --rm --force-rm --pull --network host ."
         echo "docker push"
         sh "docker push ${IMAGE_LOC}"
       }
     }
   }
   stage('Kubernetes deploy') {
     steps {
       script {
         namespace = "${env.K8S_NAMESPACE}"		  
         echo "Deploying application ${BUILD_NUMBER} to ${env.K8S_NAMESPACE} namespace"
         createNamespace (namespace)
		  sh "[ ! -z \"\$(kubectl get secret ${env.K8S_IMAGE_PULL_SECRET} -o name 2>/dev/null)\" ] || kubectl -n ${namespace}  create secret docker-registry ${env.K8S_IMAGE_PULL_SECRET} --docker-server=https://${env.IMAGE_REGISTRY} --docker-username=${env.DOCKER_USERNAME} --docker-password=${env.DOCKER_PASSWORD}"
		  sh "kubectl -n ${namespace} apply -f deployment.yaml"
         sh "kubectl -n ${namespace} rollout restart deployment/spring-deployment"
       }
     }
   }
   stage('Clear') {
     steps {
       sh "docker rmi \$(docker images -f 'dangling=true' -q) -f"
     }
   }
 }
}
